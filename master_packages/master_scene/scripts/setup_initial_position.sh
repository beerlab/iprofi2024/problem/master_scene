#!/usr/bin/env bash
initial_position=$1
if [[ "$initial_position" == "" ]]; then
  initial_position="[0.0, -1.57079632679, 1.57079632679, -1.57079632679, -1.57079632679, 0.0]"
fi
rostopic pub /joint_group_eff_controller/command std_msgs/Float64MultiArray "layout:           
  dim:                                                                                                                      
  - label: ''                                                                                                               
    size: 0                                                                                                                 
    stride: 0                                                                                                               
  data_offset: 0                                                                                                            
data: $initial_position"