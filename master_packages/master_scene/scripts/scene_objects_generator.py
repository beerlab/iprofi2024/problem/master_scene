#!/usr/bin/env python3
"""
    Based on xml (urdf) generates randomly objects (Red or Blue) in gazebo scene.
"""
import rospy
import numpy as np
import random
from gazebo_msgs.srv import SpawnModel
from geometry_msgs.msg import Pose


class ObjectsGenerator(object):

    def __init__(self):
        self.is_data_generate = False
        self.pose_file = None
        self.pose_data = []
        self.N = 1
        self.xml_model = ''
        self.geometry = []
        self.is_constraints = False
        self.object_name = 'object'

    def __del__(self):
        if self.pose_file:
            self.pose_file.close()

    def init_data(self, path, file_name, is_data_generate=False, is_constraints=False):
        self.is_constraints = is_constraints
        self.is_data_generate = is_data_generate
        if self.is_data_generate:
            self.pose_file = open(path + "/" + file_name, 'w')
        else:
            print(path + "/" + file_name)
            self.pose_data = np.genfromtxt(path + "/" + file_name + '.txt')

    def set_params(self, path, file_name, N, geometry):
        if path and file_name:
            self.read_xml_model(path + "/" + file_name)
        else:
            rospy.logerr("[Object generator] 'xml_path' failed!")
        self.N = N
        self.geometry = geometry

    def set_object_name(self, object_name):
        self.object_name = object_name

    def read_xml_model(self, xml_path):
        with open(xml_path, 'r') as xml_model_file:
            self.xml_model = xml_model_file.read().replace('\n', '')

    def get_model_with_color(self, color='Green'):
        s = self.xml_model
        return s[0:s.find('Blue')] + str(color) + s[s.find('Blue')+len('Blue'):-1]

    def loop(self):
        rospy.wait_for_service("gazebo/spawn_urdf_model")
        self.spawn = rospy.ServiceProxy("gazebo/spawn_urdf_model", SpawnModel)
        i = 0
        r = rospy.Rate(100)
        while i < self.N:

            if self.is_data_generate:

                for j in range(10):
                    x = random.random() * self.geometry[0] - self.geometry[0]/2.0 + self.geometry[3]
                    y = random.random() * self.geometry[1] - self.geometry[1]/2.0 + self.geometry[4]
                    z = random.random() * self.geometry[2]

                    # check for constraints around robot
                    if self.is_constraints:
                        if -2 <= x <= 2 or -2 <= y <= 2:
                            continue
                        else:
                            self.pose_file.write("{} {} {}\n".format(x, y, z))
                            break
                    else:
                        self.pose_file.write("{} {} {}\n".format(x, y, z))
                        break

            else:
                initial_pose = Pose()
                initial_pose.position.x = self.pose_data[i][0]
                initial_pose.position.y = self.pose_data[i][1]
                initial_pose.position.z = self.pose_data[i][2]

                try:
                    # obj_name = 'object_' + str(random.randint(0,100))
                    obj_name = self.object_name + str(abs(hash(self.pose_data[i][0] + self.pose_data[i][1] + self.pose_data[i][2])))
                    print(obj_name)
                    self.spawn.call(obj_name, self.xml_model, '', initial_pose, 'world')
                except rospy.ServiceException:
                    print("Service  call failed! ")

            i = i + 1

            r.sleep()


if __name__ == "__main__":
    rospy.init_node("scene_objects_generator_node")

    data_path = rospy.get_param('data_path')

    objgenerator_cola = ObjectsGenerator()
    objgenerator_cola_zero = ObjectsGenerator()
    objgenerator_tetra_pak_32 = ObjectsGenerator()
    objgenerator_tetra_pak_25 = ObjectsGenerator()

    # objgenerator.init_data(data_path, 'object.urdf.txt', True)  # generate data
    # objgenerator.init_data(data_path, 'object.urdf')  # read generated data
    # objgenerator.set_params(data_path, 'object.urdf', 30, (1, 1, 2, -0.5, -0.5))
    # objgenerator.loop()


    print("objects generation")
    # objgenerator.init_data(data_path, 'objects/cool-cola.urdf.txt', True)  # generate data
    objgenerator_cola.init_data(data_path, 'objects/cool-cola.urdf')  # read generated data
    objgenerator_cola_zero.init_data(data_path, 'objects/cool-cola-zero.urdf')
    objgenerator_tetra_pak_32.init_data(data_path, 'objects/tetra-pak-32.urdf')
    objgenerator_tetra_pak_25.init_data(data_path, 'objects/tetra-pak-25.urdf')

    objgenerator_cola.set_params(data_path, 'objects/cool-cola.urdf', 8, (1.5, 0.0, 0.8, -0.25, -0.2))
    objgenerator_cola.set_object_name('cool-cola')

    objgenerator_cola_zero.set_params(data_path, 'objects/cool-cola-zero.urdf', 9, (1.35, 0.0, 0.8, 0.25, 0.2))
    objgenerator_cola_zero.set_object_name('cool-cola-zero')

    objgenerator_tetra_pak_32.set_params(data_path, 'objects/tetra-pak-32.urdf', 3, (1.35, 0.0, 0.8, 0.25, 0.2))
    objgenerator_tetra_pak_32.set_object_name('tetra-pak-32')

    objgenerator_tetra_pak_25.set_params(data_path, 'objects/tetra-pak-25.urdf', 3, (1.35, 0.0, 0.8, 0.25, 0.2))
    objgenerator_tetra_pak_25.set_object_name('tetra-pak-25')

    objgenerator_cola.loop()
    objgenerator_cola_zero.loop()
    objgenerator_tetra_pak_32.loop()
    objgenerator_tetra_pak_25.loop()

    import sys; sys.exit(0)
