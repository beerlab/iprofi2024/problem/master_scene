ARG BASE_IMG

FROM ${BASE_IMG} as main
LABEL org.opencontainers.image.authors="texnoman@itmo.com"

SHELL ["/bin/bash", "-c"]
ENV DEBIAN_FRONTEND noninteractive

ENV GAZEBO_GUI false
ENV RVIZ_GUI false

RUN apt update \
    && apt install -y ros-${ROS_DISTRO}-gazebo-ros-control \
        ros-${ROS_DISTRO}-gazebo-ros-pkgs \
    && rm -rf /var/lib/apt/lists/* && apt autoremove && apt clean
RUN mkdir src/master_packages
COPY ./master_packages/ src/master_packages/
RUN catkin build

HEALTHCHECK --interval=20s --timeout=1s --retries=3 --start-period=20s CMD if [[ $(rostopic list 2> /dev/null | wc -c) > 0 ]]; then exit 0; fi;
CMD ["/bin/bash", "-ci", "roslaunch master_scene start_scene.launch gazebo_gui:=${GAZEBO_GUI} rviz_gui:=${RVIZ_GUI}"]

FROM main as xvfb
RUN apt-get update && apt-get install --no-install-recommends -y \
    supervisor glmark2 ca-certificates xvfb && \
    rm -rf /var/lib/apt/lists/*

# Install VirtualGL and make libraries available for preload
ARG VIRTUALGL_URL="https://sourceforge.net/projects/virtualgl/files"
ARG VIRTUALGL_VERSION=3.1
RUN curl -fsSL -O "${VIRTUALGL_URL}/virtualgl_${VIRTUALGL_VERSION}_amd64.deb" && \
    curl -fsSL -O "${VIRTUALGL_URL}/virtualgl32_${VIRTUALGL_VERSION}_amd64.deb" && \
    apt-get update && apt-get install -y --no-install-recommends ./virtualgl_${VIRTUALGL_VERSION}_amd64.deb ./virtualgl32_${VIRTUALGL_VERSION}_amd64.deb && \
    rm -f "virtualgl_${VIRTUALGL_VERSION}_amd64.deb" "virtualgl32_${VIRTUALGL_VERSION}_amd64.deb" && \
    rm -rf /var/lib/apt/lists/* && \
    chmod u+s /usr/lib/libvglfaker.so && \
    chmod u+s /usr/lib/libdlfaker.so && \
    chmod u+s /usr/lib32/libvglfaker.so && \
    chmod u+s /usr/lib32/libdlfaker.so && \
    chmod u+s /usr/lib/i386-linux-gnu/libvglfaker.so && \
    chmod u+s /usr/lib/i386-linux-gnu/libdlfaker.so

ENV DISPLAY :0
ENV SIZEW 640
ENV SIZEH 480
ENV REFRESH 30
ENV CDEPTH 16
ENV VGL_DISPLAY=${VGL_DISPLAY:-egl}
ENV VGL_REFRESHRATE=$REFRESH

COPY entrypoint.sh /etc/entrypoint.sh
RUN chmod 755 /etc/entrypoint.sh
COPY supervisord.conf /etc/supervisord.conf
RUN chmod 755 /etc/supervisord.conf

HEALTHCHECK --interval=20s --timeout=1s --retries=3 --start-period=20s CMD if [[ $(rostopic list 2> /dev/null | wc -c) > 0 ]]; then exit 0; fi;
CMD ["/bin/bash", "-ci", "/usr/bin/supervisord & sleep 5; vglrun roslaunch master_scene start_scene.launch gazebo_gui:=${GAZEBO_GUI} rviz_gui:=${RVIZ_GUI}"]

